\documentclass[../main.tex]{subfiles}
\begin{document}

In order to model the coupling that occur within the individual parts of the visual cortex
we need to preprocess the data in order to extract the needed information that describe
the underlying interactions. The phases of the individual phases are estimated using the
Hilbert Transform in order to transform proto-phases with the proto-phase-phase $\theta \rightarrow \phi$ transform~\cite{kralemann2008phase}.
The block diagram of the steps taken are displayed in figure~\ref{fig:block_diagram}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth, height=14cm]{images/flowchart/flowchart.pdf}
	\caption{Block diagram}\label{fig:block_diagram}
\end{figure}

\subsection{Hilbert Transform}
The Hilbert Transform is a linear operator just like the Fourier transform. The main
advantage of using the Hilbert Transform is it's ability for analysis of signals
of non-stationary and nonlinear systems. Thought transforming the signals as a function of change
of their phase we gain insight in the signals frequency as a time-dependent function.
The Hilbert Transform of a signal $U(t)$ (figure~\ref{fig:hilbert_tf} A)  is a complex function of
shape $H(u)(t)$:
\begin{equation}
	H(u)(t) = \frac{1}{\pi} \int_{-\infty}^{\infty} \frac{u(\tau)}{t-\tau} d\tau
\end{equation}

The Hilbert Transform in the frequency domain is represented as a phase displaced Fourier spectrum.
Every Fourier component  has a phase-displacement of $-90^{\circ}$.
The Hilbert Transform is a very important tool in signal processing because it can represent
the analytical signal of a given real signal $u(t)$.

The analytical signal (Figure\ref{fig:hilbert_tf} B) of a real signal is represented as:
\begin{equation}
	U_{a}(t) = u(t) + iv(t)
\end{equation}

Where $u(t)$ represents the signal itself, while $v(t)$ represents the Hilbert Transform
of the given signal. The sequence of digitalized values of the analytical signal in the complex
plane represent a vector that rotates counter-clockwise in the complex plane.
One rotating vector in the complex plane is represented with it's amplitude and phase.
The Analytical phase (figure~\ref{fig:hilbert_tf} A) is described by:
\begin{equation}
	A(t) = \sqrt{u^{2}(t) + v^{2}(t)}
\end{equation}

The analytical phase (figure~\ref{fig:hilbert_tf} C) of the system is described by the relation:
\begin{equation}
	A(t) = \arctan{\frac{v(t)}{u(t)}}
\end{equation}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/hilbert_plots/hilbert_fig.png}
	\caption{Application of the Hilbert Transform on EEG data}\label{fig:hilbert_tf}
\end{figure}

\subsection{Proto-Phase $\rightarrow$ Phase transform}
With the use of the Hilbert Transform we can find the analytical phase of the signals that
we analyze. The analytical phase is also called the proto-phase.
The proto-phase is a variable that  monotonically grows along the
phase trajectory of the oscillator, but generally uniformly\cite{kralemann2008phase}.
The proto-phase is not invariant and it is dependent on the choice of the initial
values of the system\cite{kralemann2007uncovering}.

When analyzing coupled oscillators that are in a coupled state their phase
interaction gets the shape:
\begin{equation}
	\dot{\phi_{1}} = \omega_{1} + f(\theta) + \hat{q}(\theta_{1}, \theta_{2})
\end{equation}

The relation can also be represented by the independent phases of each
of the oscillators.
\begin{equation}
	\dot{\phi_{1}} = \omega_{1} + q(\phi_{1}, \phi_{2})
\end{equation}

Because of the nature of the proto-phases to manifest themselves as distorted phases they
add noise to the system that we are inferring from data. The $f(\theta)$ term
is of interest because it has no physical meaning and masks the term $\hat{q}(\theta_{1}, \theta_{2})$
because of it's small amplitudes.

The phase is a continuous variable along the phase trajectory of the phase dynamics
of a given oscillatory system. The phase has monotonic and uniform growth represented
through the relation $\dot{\phi} = \mbox{const}$. The phase is a invariant variable
and it is not dependent on the initial conditions of our oscillatory system.

For a given system which does not include noise the equation~\ref{eq:proto_to_phase} is valid.
\begin{equation}
	\begin{aligned}
		\frac{d\phi}{dt} = \omega_{0}                                      \\
		\frac{d\phi}{d\theta} \frac{\theta}{dt} = \omega_{0}               \\
		\frac{d\phi}{d\theta} = \frac{\omega_{0}}{\dot{\theta}}            \\
		\phi = \omega \int_{0}^{\theta}\frac{d\theta}{\dot{\theta}}        \\
		\phi = \omega \int_{0}^{\theta}\frac{dt}{d\theta}(\theta')d\theta' \\
	\end{aligned}\label{eq:proto_to_phase}
\end{equation}

In noise or chaotic data the equation~\ref{eq:proto_to_phase} is invalid because
the phase trajectory of these kind of systems is not a closed contour by which
the dynamics of the system periodically manifests itself.


In order to infer the equation of interest the mean of the right side of the
equation~\ref{eq:proto_to_phase} is calculated:
\begin{equation}
	\frac{d\phi}{d\theta} = w_{0}\langle \frac{dt}{d\theta}(\theta)\rangle = \sigma(\theta)
	\label{eq:initial_transform}
\end{equation}

The equation~\ref{eq:proto_to_phase} in cases that we have a big number of data points
can be substituted by integration by time along the trajectory of the oscillator:

\begin{equation}
	\langle \frac{dt}{d\theta}(\theta)\rangle = \frac{1}{T} \int_{0}^{T} \frac{dt}{d\theta}(\theta)
\end{equation}

The equation that is represented by $\frac{1}{2\pi} \sigma(\theta)$  represents
the probability density of the phase of the system $\theta$ because of it's
inverse proportional to the speed of every point during the trajectory of
the oscillator~\cite{kralemann2008phase}. The probability density of
the variable $\theta$ is given:
\begin{equation}
	\frac{1}{2\pi} \sigma(\theta) = \langle \delta(\Theta(t) - \theta)
	\label{eq:prob_dist}
\end{equation}

If we represent the probability density as a Fourier series it can be
written down like:
\begin{equation}
	\sigma(\theta) = \sum_{n}S_{n} e^{in\theta}\label{eq:calculating_the_dist}
\end{equation}

With coefficients:
\begin{equation}
	\sigma(\theta) = \frac{1}{2\pi} \int_{0}^{2\pi}\sigma(\theta)e^{-in\theta} d\theta
	\label{eq:prob_fourier_coefs}
\end{equation}

If substituting the equation~\ref{eq:prob_dist} in equation~\ref{eq:prob_fourier_coefs}
and we substitute the integration by a time dependent sum we get:
\begin{equation}
	S_{n} = \frac{1}{T} \int_{0}^{T}dt \int_{0}^{2\pi} d\theta e^{-in\theta} \delta(\Theta(t) - \theta) = \frac{1}{T} \int_{0}^{T} e^{-in\Theta(t)} dt
\end{equation}
\begin{equation}
	S_{n} = \sum_{i=1}^{N} e^{-in\Theta(t_{i})}
\end{equation}

And at the end we can substituted equation~\ref{eq:calculating_the_dist}
and~\ref{eq:prob_fourier_coefs} to find the transform of interest.

\end{document}
