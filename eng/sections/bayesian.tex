\documentclass[../main.tex]{subfiles}
\begin{document}

\subsection{Model of interactions}
Dynamical Bayesian inference is a method used for inferring the parameters of a
model given a system of differential equations in the state space through
analysis of time series of data. Once the model parameters have been inferred
with the use of Bayes theorem the parameter distribution could be improved.

Biological signals are multidimensional by nature. In order to simplify the
analysis of the signals a transform is required which could transform the
signals in the phase space. The periodicity and oscillatory nature of EEG signals
allows for analysis of their phase dynamics. In the most basic case we analyze a
system of $N$ coupled oscillators.

\begin{equation}
	\dot{\phi_i} = \omega_i + q_i(\phi_1, \phi_2, \dots, \phi_n, t) + \xi_i(t)
\end{equation}

The term $\dot{\phi_i}$ represents the first derivative of the phase with
respect to time,  $\omega_i$ represents the natural frequency of the system
while  $\xi_i$ represents stochastic perturbations in the form of noise for
which  we assume that take on the form as Gaussian processes and are modeled as
white Gaussian noise.

The relationship that the terms $\omega_i$ and $q(\phi_i)$ have is a relation of
acceleration and deceleration of the natural frequency of the given oscillator.
If the term $q(\phi_i)$ has a positive value we observe an interaction based on
acceleration of the natural frequency of the oscillator. If a negative value is
present we observe deceleration of the natural frequency of the
oscillator~\cite{stankovski2017coupling}. The shape of the coupling function is
based on the relation of acceleration that the oscillators share. The importance
of the acceleration of the phase dynamics that the oscillators have is important
in a sense that it represents the evolution of the coupling interaction present
in the system\cite{stankovski2017neural}.

\subsection{Dynamic Inference}

The parameters that are inferred are represented by a stochastic differential
equation represented as:
\begin{equation}
	\dot{x_i} = f(x_i, x_j\rvert c) + \sqrt D \xi_i
	\label{eq:stohastic_model}
\end{equation}

Because all of the function are periodic by nature we make the assumption that
they could be represented by a finite number of Fourier components~\cite{kralemann2011reconstructing}
\begin{equation}
	\dot{\phi_i} = \sum_{k=-K}^{K} c^{(i)}_k \Phi_{i,k}(\phi_1, \phi_2, \dots, \phi_N)
	+ \xi_i = \sum_{k=-K}^{K} e^{i(k1\phi_1+k2\phi_2+ \cdots +k_N \phi_N)}
\end{equation}

For a given time series that represent data that take on the shape of
$2 \times M$ with the help of dynamic Bayesian inference our goal is to maximize
the posterior conditional probability. The conditional probability is
represented as  $P_\mathcal{X}(\mathcal{M}\rvert \mathcal{X})$ where the
parameters are given by the term $\mathcal{M}$  for given data $\mathcal{X}$.
The Bayesian probability is represented as:
\begin{equation}
	P_\mathcal{X}(\mathcal{M} \rvert \mathcal{X}) = \frac{\ell(\mathcal{X} \rvert \mathcal{M})
		p_{prior}(\mathcal{M})}{\int\ell(\mathcal{X} \rvert \mathcal{M}) p_{prior}(\mathcal{M}) d
		\mathcal{M}}
\end{equation}

The discretization of the problem is possible using the Euler mid point
approximation:
\begin{equation}
	x^{*}_n = \frac{(x_{n+1} + x_{b})}{h}
\end{equation}

With discretization of~\ref{eq:stohastic_model} can be written as:
\begin{equation}
	x_{i,n+1} = x_{i,n}  + h(x^*_{i, n}, x^*_{j, n} \rvert c) + h \sqrt D z_n
\end{equation}

$z_n$ represents the solution of the stochastic integral:
\begin{equation}
	z_n \equiv \int_{t}^{t+1} z(t) dt
\end{equation}

The negative log likelihood probability given as:
\begin{multline}
	S = \frac{N}{2} + \ln \left|D\right| + \frac{h}{2}
	\sum_{n=0}^{N-1}(c_k \frac{\partial P(x_., n)}{\partial x} + \\
	+  {\left[\dot{x}_n - c_k f_k(x^{*}_., n) \right]}^{T}  (D^{-1})
	\left [\dot{x}_n - c_k f_k(x^{*}_., n) \right])
\end{multline}

The minimization of the log negative likelihood function maximizes the
probability function. The log negative likelihood function and the probability
function share the same extreme point. The log negative function has a minimum
in a point that the probability function has a maximum. For the assumed normal
distribution for the parameters  $c$, with  mean $\bar{c}$ and a matrix
covariance  $\Sigma_{prior} = \Xi^{-1}_{prior}$ the posterior probability can be
calculated:
\begin{equation}\label{equ:b1}
	D = \frac{h}{N} {(\dot{x} - c_k f_k (x^{*}_{.,n}))}^{T}(\dot{x} - c_k f_k (x^{*}_{.,n}))
\end{equation}

\begin{equation}\label{equ:b2}
	c_k = {(\Xi^{-1})}_{kw} r_{wk}
\end{equation}

\begin{equation}\label{equ:b3}
	r_w = {(\Xi_{prior})}_{kw} c_{kw} + h f_k(x^{*}_{.,n} ) D^{-1} \dot(x_n) \pm \frac{h}{2} \frac{\partial
		f_k(x_{.,n})}{\partial x}
\end{equation}

\begin{equation}\label{equ:b4}
	r_w = {(\Xi_{prior})}_{kw}  + h f_k(x^{*}_{.,n} ) D^{-1} f_w (x^{*}_{.,n})
\end{equation}

The proposed approach makes the calculation on time windows of size $w$. For
every time window of size $w$ equations~\ref{equ:b1} to~\ref{equ:b4} are
applied. Every concurrent time window of calculation uses the prior distribution
$\Xi$ altered in a way that separates the oscillatory dynamics from the system
noise. In order to achieve the separation of system dynamics from noise we
introduce a diagonal matrix $\Xi_{diff}^{n}$ that contains the value of the
posterior distribution $\sigma_i$ scales with a propagation constant $p_{w}$.
With the introduction of the matrix $\Xi_{diff}$ we get the
relation~\ref{eq:prior_posterior_diff} that displays the relation between the
prior and posterior distribution:

\begin{equation}
	\label{eq:prior_posterior_diff}
	\Xi_{prior}^{n+1} =  \Xi_{posterior}^{n} + \Xi_{diff}^{n}
\end{equation}

\subsection{Coupling Strength}

The coupling strength quantitatively describes the coupling function with
respect to the strength of the interaction of the oscillators. The coupling
strength is defined as the second norm of the inferred parameters. The second
norm is used because of the Fourier decomposition of the inferred parameters $c$.
\begin{equation}
	\left\Vert q_i\right \Vert = \langle q_i q_i\rangle ^ {1/2}
\end{equation}

The direction of the coupling is defined by the relation:
\begin{equation}
	\rho(q_i, q_j) = \frac{\left\Vert q_i\right \Vert - \left\Vert q_j \right \Vert}{\left\Vert q_i\right \Vert + \left\Vert q_j \right \Vert}
\end{equation}

\subsection{Coupling strength in channel to channel interactions}

Figures~\ref{fig:heatmap5},~\ref{fig:heatmap6} and~\ref{fig:heatmap10} represent
heat maps from averaged values of coupling strength in experiments conducted at a
frequency $5 Hz$, $6 Hz$ and $10 Hz$ respectively.

From the coupling maps we can infer that the maximum amplitude in
$q_2 \rightarrow q_1$ coupling interactions is present between channels $PO3$
and $O2$. In  $q_1 \rightarrow q_2$ coupling interactions channels $O2$ and $O1$
display maximum coupling strength. The above mentioned occurrence is present in
all experiment frequencies.

In the $q_{1} \rightarrow q_{2}$ interaction probe $P8$ displays constant
coupling strength when in interaction with the rest of the probes for the tree
experimental frequencies.

What we can deduce from the experiments at frequency of $10Hz$ is the occurrence
of drastically raising of the amplitude of the interactions between
$q_1 \rightarrow q_2$ between the probes $O2$ and $O1$.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/coupling_strength_heatmaps/five_hz_heatmap.png}
	\caption{Coupling strengths in experiments of $5 Hz$}\label{fig:heatmap5}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/coupling_strength_heatmaps/six_hz_heatmap.png}
	\caption{Coupling strengths in experiments of $6 Hz$}\label{fig:heatmap6}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/coupling_strength_heatmaps/ten_hz_heatmap.png}
	\caption{Coupling strengths in experiments of $10 Hz$}\label{fig:heatmap10}
\end{figure}

Figures~\ref{fig:heatmap_a} and~\ref{fig:heatmap_b} represent heat maps of mean
values of coupling strengths in the two states that the experiments was held.
The first state is the first trial of the experiment while the second
state is the second trial of the experiment held after a prolonged period of
time.

The two figures elucidate the assumption that the time interval has influence on
the strengths on the coupling that the probes have. Maximum coupling strengths
in the probes do not differ from the previous mean coupling strengths based on
the frequency of the experiment.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/coupling_strength_heatmaps/a_experiment_cmap.png}
	\caption{Coupling strength in the first trial of the experiment}\label{fig:heatmap_a}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/coupling_strength_heatmaps/b_experiment_cmap.png}
	\caption{Coupling strengths in the second trial of the experiment}\label{fig:heatmap_b}
\end{figure}

\subsection{Synchronization index in probe combinations}
Synchronization is a phenomena that occurs in dynamic oscillatory systems that
are coupled. Synchronization manifests in self with the rate of change of the
rhythm of the oscillations in autonomous oscillators. We can observe two
manifestations of synchronization: phase-locking and frequency-locking.

Before applying the dynamical Bayesian inference method as means of
identification of the model of coupling interactions, we calculate the mean
phase coherence index of the phases.

The mean phase coherence index is based on the circular variance of an angular
distribution. The index is calculated by projecting the phase difference of all
the oscillators on the unit circle and calculating the mean value of the mean
phase difference between the phases~\cite{kuramoto2003chemicals, mormann2000mean}.


\begin{figure}[H]
	\begin{subfigure}[t]{.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]
		{images/phase_synchronization_unit_circle/unit_circle_0_38.png}
		\caption{Synchronization index of 0.38}\label{fig:weak_synch}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.49\textwidth}
		\centering
		\includegraphics[width=\linewidth]
		{images/phase_synchronization_unit_circle/unit_circle_0_78.png}
		\caption{Synchronization index of 0.78}\label{fig:strong_synch}
	\end{subfigure}
	\caption{Mean phase coherence unit circle.}
\end{figure}

The mean phase coherence index as a measure of synchronization is given by the equation:

\begin{equation}
	r_{n,m} =\left| \frac{1}{N} \sum_{i=0}^{N-1} e^{\imath(\theta_{n}(t_i) - \theta_{m}(t_i))}  \right|
\end{equation}

The mean phase coherence index is confined on the interval [0,1]. Mean phase
coherence index near one is attained from phases that are in synchronization
while a value of zero is attained by phases that have no synchronization. If
phase synchronization is present in the data observed applying the method of
dynamic Bayesian inference can not obtain the model parameters.
\begin{figure}[H]
	\begin{subfigure}[t]{.47\textwidth}
		\centering
		\includegraphics[width=\linewidth]{synchronization_histograms/mean_phase_coherence_index_of_all_subjects.png}
		\caption{Mean phase coherence index of the whole data set}\label{fig:all_sync_index}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{.47\textwidth}
		\centering
		\includegraphics[width=\linewidth]{/synchronization_histograms/mean_phase_coherence_index_of_failed_subjects.png}
		\caption{Mean phase coherence index in data samples that could not be inferred}\label{fig:failed_sync_index}
	\end{subfigure}
	\caption{Distribution of mean phase coherence indexes on the whole data set}
\end{figure}

From the 1736 combinations present in the EEG data the dynamic Bayesian
Inference could inferred the phase dynamics from 1462 combinations while 274
combinations were unsuccessful.

Figure~\ref{fig:all_sync_index} shows the distribution of the synchronization
indexes calculated on the whole data set. The mean value of the mean phase
coherence index is $\mu = 0.3996$, while the standard deviation has a value
of $\sigma = 0.1917$.

Figure~\ref{fig:failed_sync_index} shows the distribution of the synchronization
indexes calculated on data samples that the dynamic Bayesian inference could not
infer. The mean value of the mean phase
coherence index is $\mu = 0.6306$, while the standard deviation has a value
of $\sigma = 0.1938$. The reason why the phases of the EEG signals are highly
synchronized is that the probes that are placed on the subjects scalps are close
to each other and as such two distinct probes could sense the same neural activity.

\subsection{Results from the Dynamic Bayesian Inference}

Using DBI the coupling parameters of the coupling functions are inferred.
With the model and the parameters we can simulate the coupling model
of the oscillators and analyze the properties of the coupling.

In the majority of the coupling functions there exists strong diagonalization
of the shape of the functions which implies the existence of a coupling interaction
between the two oscillators, the uni-laterality of the shape of the coupling function
implies absence from the mutual interactions of the oscillators~\cite{stankovski2015coupling}.

Figures~\ref{fig:coupling12} and~\ref{fig:coupling24} represent the median coupling functions
for the first experiment and the second one made after a long pause after the first experiment.
The median coupling functions are coupling function that describe the general interaction
present in all of the subjects~\cite{stankovski2015coupling}.

We can notice that we have a considerable similarity between the coupling functions in
both interactions of the experiment.  We can conclude that there is no substantial
difference in the coupling during the first and the second experiment of the RSVP task.

% TODO: CHECK THIS THIS IS SUS
Figure~\ref{fig:coupling12} shows the coupling function of probes $PO7$ and $PO8$.
What is characteristical about this interaction is the lowering of the value of the
coupling function of the $PO7$ probe based on the oscillations of the $PO8$.
This occurs only on the interval $[0 - \frac{\pi}{2}]$. This interaction is
characteristical for every probe pairs except for pairs $(P7, P8), (O1, O2)$ and
$(PO4, PO3)$. The characteristical deceleration is present in all of the coupling
interactions between probes $PO8$ and $PO7$ on the frequency interval of $[\pi - 2\pi]$.

Figure~\ref{fig:coupling24} shows the coupling function of probes $P7$ and $PO7$.
The difference that this pair display is the lowering of the value of the
coupling function in a wider interval of values $[0 - \pi]$. Other probes that
show the same occurrence are $(P7, P8), (O1, O2)$ and $(PO4, PO3)$.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/state_images/mean_c12.png}
	\caption{Before and after states of the coupling function between probes PO7 and PO8}\label{fig:coupling12}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/state_images/mean_c24.png}
	\caption{Before and after states of the coupling function between probes P7 and PO7}\label{fig:coupling24}
\end{figure}

Figures~\ref{fig:mcoupling12}-\ref{fig:mcoupling68} represent some
characteristic coupling functions. The displayed coupling functions have
their amplitudes averaged by the frequency at which the experiment was done.
We can conclude that we don't have a significant qualitative fluctuation between
the displayed coupling functions. The main difference that we can observe
is the different amplitudes of the coupling functions depending on the frequencies
that the visual experiment was held at.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/summary_of_frequency/same_plotc12.png}
	\caption{Interaction between probes $PO7$ and $PO8$ for different frequencies of the visual experiment}\label{fig:mcoupling12}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/summary_of_frequency/same_plotc56.png}
	\caption{Interaction between probes $O2$ and $O1$ for different frequencies of the visual experiment}\label{fig:mcoupling56}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{images/summary_of_frequency/same_plotc68.png}
	\caption{Interaction between probes $PO4$ and $O2$ for different frequencies of the visual experiment}\label{fig:mcoupling68}
\end{figure}

\end{document}
